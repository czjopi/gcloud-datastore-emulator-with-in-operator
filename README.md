## Run with datastore-emulator - firestore in datastore mode enabled

```
docker compose up
...
2022/07/15 20:49:19 rpc error: code = InvalidArgument desc = Filter has 2 properties, expected 1
```

## Run with real datastore - firestore in datastore mode

```
export DATASTORE_PROJECT_ID=<project_id>
go run main.go
...
2022/07/15 22:46:50 Id: 2	 Name: Matylda
2022/07/15 22:46:50 Id: 1	 Name: Samuel
```
