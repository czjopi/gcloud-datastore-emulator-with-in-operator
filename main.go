package main

import (
	"context"
	"log"
	"os"

	"cloud.google.com/go/datastore"
)

type User struct {
	Name string
	Id   string
}

func main() {
	ctx := context.Background()

	projectID := os.Getenv("DATASTORE_PROJECT_ID")

	client, err := datastore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()

	keys := []*datastore.Key{
		datastore.IncompleteKey("User", nil),
		datastore.IncompleteKey("User", nil),
	}

	users := []*User{
		{Name: "Samuel", Id: "1"},
		{Name: "Matylda", Id: "2"},
	}
	if _, err := client.PutMulti(ctx, keys, users); err != nil {
		log.Fatal(err)
	}

	ids := []string{"1", "2"}
	idsInterface := make([]interface{}, 0, len(ids))
	for _, i := range ids {
		idsInterface = append(idsInterface, i)
	}
	q := datastore.NewQuery("User").
		FilterField("Id", "in", idsInterface)

	usersGet := make([]*User, 0)
	_, err = client.GetAll(ctx, q, &usersGet)
	if err != nil {
		log.Fatal(err)
	}

	if len(usersGet) > 0 {
		for u := range usersGet {
			log.Printf("Id: %s\t Name: %s\n", usersGet[u].Id, usersGet[u].Name)
		}
	}
}
