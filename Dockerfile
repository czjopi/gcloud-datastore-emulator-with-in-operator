FROM golang:1.18-alpine3.15
WORKDIR /app
COPY . .
RUN apk add --no-cache git
RUN go build -o main .
CMD ["/app/main"]
